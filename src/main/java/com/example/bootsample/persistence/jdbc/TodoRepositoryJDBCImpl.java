package com.example.bootsample.persistence.jdbc;

import com.example.bootsample.entity.Todo;
import com.example.bootsample.persistence.TodoRepository;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class TodoRepositoryJDBCImpl implements TodoRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;

    public TodoRepositoryJDBCImpl(NamedParameterJdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Todo> findAll() {
        return jdbcTemplate.query(
                "SELECT id, text, done FROM todo ORDER BY id",
                (rs, rowNum) -> new Todo(
                        rs.getLong("id"),
                        rs.getString("text"),
                        rs.getBoolean("done"))
        );
    }

    @Override
    public Todo findById(long id) {
        return jdbcTemplate.queryForObject(
                "SELECT id, text, done FROM todo WHERE id = :id",
                getIdParamMap(id),
                (rs, rowNum) -> new Todo(
                        rs.getLong("id"),
                        rs.getString("text"),
                        rs.getBoolean("done")
                ));
    }

    @Override
    public void insert(String newTodoText) {
        String sql = "INSERT INTO todo (text, done) VALUES (:text, :done)";
        MapSqlParameterSource parameterSource = new MapSqlParameterSource();
        parameterSource.addValue("text", newTodoText);
        parameterSource.addValue("done", false);
        jdbcTemplate.update(sql, parameterSource);
    }

    @Override
    public void changeDoneTrue(long id) {
        jdbcTemplate.update("UPDATE todo SET done = true where id = :id", getIdParamMap(id));
    }

    @Override
    public void delete(long id) {
        jdbcTemplate.update("DELETE FROM todo WHERE id = :id", getIdParamMap(id));
    }

    private Map<String, Long> getIdParamMap(long id) {
        Map<String, Long> idParamMap = new HashMap<>();
        idParamMap.put("id", id);
        return idParamMap;
    }

}
